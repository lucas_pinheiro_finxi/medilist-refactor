/*

FINXI TECNOLOGIA SA

Backend code written to work with Firebase
Author: Lucas Pinheiro

*/

const functions = require('firebase-functions');
const admin = require('firebase-admin');

//

admin.initializeApp();

//

exports.refact = functions.https
    .onRequest(async (req, res) => {
        async function run() {
            // Function that checks if a field is undefined
            function protect(value) {
                return value === undefined ? null : value;
            }
            // Function that converts date to the new format
            function convertDateToNewFormat(validity) {
                if (!validity || !validity.year || !validity.month)
                    return new Date().toISOString();
                let year = validity.year;
                let month = validity.month;
                let yearStr, monthStr;
                if (typeof year === "string")
                    yearStr = year;
                else
                    yearStr = "" + year;
                if (typeof month === "string")
                    monthStr = month.length > 1 ? month : "0" + month;
                else
                    monthStr = month < 10 ? "0" + month : "" + month;
                let time = `${yearStr}-${monthStr}-01`;
                return new Date(time).toISOString();
            }
            // Load database
            let db = admin.database();
            // Create key mappings
            let batchMap = {};
            let eanMap = {};
            let healthChallengesMap = {};
            let medicineMap = {};
            let patientMap = {};
            let pharmacyMap = {};
            let supplyMap = {};
            let userMap = {};
            // Move snapshot into /old
            try {
                console.log("[REFACTOR] Moving database snapshot into /old");
                let snapshot = await db.ref("/").once('value');
                await db.ref("/").update({ old: snapshot.val(), new: {} });
            } catch (e) {
                console.error("[REFACTOR] Cloud function error: function refact 0");
                console.error(e);
                return;
            }
            // Refactor users
            try {
                console.log("[REFACTOR] Refactoring users");
                let promiseList = [];
                let usersSnapshot = await db.ref("/old/users").once('value');
                usersSnapshot.forEach(userSnapshot => {
                    let oldUserKey = userSnapshot.key;
                    let newUserKey = db.ref("/new/users").push().key;
                    userMap[oldUserKey] = newUserKey;
                    let user = userSnapshot.val();
                    let promise = db.ref(`/new/users/${newUserKey}`).set({
                        birthday: protect(user.birthday),
                        createdOn: protect(user.createdOn),
                        email: protect(user.email),
                        gender: protect(user.gender),
                        isAcceptedTerms: protect(user.isAcceptedTerms),
                        isRegisterCompleted: protect(user.isCompletedRegister),
                        name: protect(user.name),
                        photoURL: protect(user.photoUrl),
                        settings: { notificationSound: true, notificationVibration: true }
                    });
                    promiseList.push(promise);
                });
                await Promise.all(promiseList);
            } catch (e) {
                console.error("[REFACTOR] Cloud function error: function refact 1");
                console.error(e);
                return;
            }
            // Refactor healthChallenge
            try {
                console.log("[REFACTOR] Refactoring healthChallenge");
                let promiseList = [];
                let healthChallengesSnapshot = await db.ref("/old/healthChallenges").once('value');
                healthChallengesSnapshot.forEach(healthChallengeSnapshot => {
                    let oldHealthChallengeKey = healthChallengeSnapshot.key;
                    let newHealthChallengeKey = db.ref("/new/healthChallenges").push().key;
                    healthChallengesMap[oldHealthChallengeKey] = newHealthChallengeKey;
                    let healthChallenge = healthChallengeSnapshot.val();
                    let promise = db.ref(`/new/healthChallenges/${newHealthChallengeKey}`).set({
                        name: protect(healthChallenge.name)
                    });
                    promiseList.push(promise);
                });
                await Promise.all(promiseList);
            } catch (e) {
                console.error("[REFACTOR] Cloud function error: function refact 2");
                console.error(e);
                return;
            }
            // Refactor patients
            try {
                console.log("[REFACTOR] Refactoring patients");
                let promiseList = [];
                let patientsSnapshot = await db.ref("/old/patients").once('value');
                patientsSnapshot.forEach(patientSnapshot => {
                    let oldPatientKey = patientSnapshot.key;
                    let newPatientKey = db.ref("/new/patients").push().key;
                    patientMap[oldPatientKey] = newPatientKey;
                    let patient = patientSnapshot.val();
                    let promise = db.ref(`/new/patients/${newPatientKey}`).set({
                        birthdate: protect(patient.birthdate),
                        bloodType: protect(patient.bloodType),
                        gender: protect(patient.gender),
                        name: protect(patient.name),
                        notes: protect(patient.notes)
                    });
                    promiseList.push(promise);
                })
                await Promise.all(promiseList);
            } catch (e) {
                console.error("[REFACTOR] Cloud function error: function refact 3");
                console.error(e);
                return;
            }
            // Refactor patients x healthChallenges relationship
            try {
                console.log("[REFACTOR] Refactoring patients x healthChallenges relationship");
                let promiseList = [];
                let relationshipsSnapshot = await db.ref("/old/patients_healthChallenges").once('value');
                relationshipsSnapshot.forEach(relationshipSnapshot => {
                    let relationship = relationshipSnapshot.val();
                    let oldPatientKey = relationship.patientId;
                    let newPatientKey = patientMap[oldPatientKey];
                    let oldHealthChallengeKey = relationship.healthChallengeId;
                    let newHealthChallengeKey = healthChallengesMap[oldHealthChallengeKey];
                    if (newPatientKey === null || newPatientKey === undefined) {
                        console.log(`[REFACTOR] Patients_x_healthChallenges: Patient id='${oldPatientKey}' was not found`);
                    } else if (newHealthChallengeKey === null || newHealthChallengeKey === undefined) {
                        console.log(`[REFACTOR] Patients_x_healthChallenges: Health Challenge id='${oldHealthChallengeKey}' was not found`);
                    } else {
                        let prom = db.ref(`/new/patients/${newPatientKey}/healthChallenges/${newHealthChallengeKey}`).set(true);
                        promiseList.push(prom);
                    }
                });
                await Promise.all(promiseList);
            } catch (e) {
                console.error("[REFACTOR] Cloud function error: function refact 4");
                console.error(e);
                return;
            }
            // Refactor patients x user relationship
            try {
                console.log("[REFACTOR] Refactoring patients x user relationship");
                let promiseList = [];
                let patientsSnapshot = await db.ref("/old/patients").once('value');
                let patientsList = [];
                patientsSnapshot.forEach(patientSnapshot => { patientsList.push(patientSnapshot.key) });
                for (let i = 0; i < patientsList.length; ++i) {
                    let oldPatientKey = patientsList[i];
                    let careTakersSnapshot = await db.ref(`/old/patients/${oldPatientKey}/careTakers`).once('value');
                    careTakersSnapshot.forEach(careTakerSnapshot => {
                        let careTaker = careTakerSnapshot.val();
                        let oldUserKey = careTaker.id;
                        let newUserKey = userMap[oldUserKey];
                        let newPatientKey = patientMap[oldPatientKey];
                        if (newUserKey === null || newUserKey === undefined) {
                            console.log(`[REFACTOR] Patients_x_users: Patient id='${oldUserKey}' was not found`);
                        } else if (newPatientKey === null || newPatientKey === undefined) {
                            console.log(`[REFACTOR] Patients_x_users: Patient id='${oldPatientKey}' was not found`);
                        } else {
                            let relKey = db.ref("/new/user_patient").push().key;
                            let prom = db.ref(`/new/user_patient/${relKey}`).set({
                                patientKey: newPatientKey,
                                userKey: newUserKey,
                                relationKey: `${newUserKey}_x_${newPatientKey}`,
                                isOwner: careTaker.owner
                            });
                            promiseList.push(prom);
                        }
                    });
                }
                await Promise.all(promiseList);
            } catch (e) {
                console.error("[REFACTOR] Cloud function error: function refact 5");
                console.error(e);
                return;
            }
            // Refactor pharmacies
            try {
                console.log("[REFACTOR] Refactoring pharmacies");
                let promiseList = [];
                let pharmaciesSnapshot = await db.ref("/old/pharmacies").once('value');
                pharmaciesSnapshot.forEach(pharmacySnapshot => {
                    let oldPharmacyKey = pharmacySnapshot.key;
                    let newPharmacyKey = db.ref("/new/pharmacies").push().key;
                    pharmacyMap[oldPharmacyKey] = newPharmacyKey;
                    let pharmacy = pharmacySnapshot.val();
                    let promise = db.ref(`/new/pharmacies/${newPharmacyKey}`).set({
                        name: protect(pharmacy.name)
                    });
                    promiseList.push(promise);
                });
                await Promise.all(promiseList);
            } catch (e) {
                console.error("[REFACTOR] Cloud function error: function refact 6");
                console.error(e);
                return;
            }
            // Refactor medicines
            try {
                console.log("[REFACTOR] Refactoring medicines");
                let promiseList = [];
                let pharmaciesList = [];
                let pharmaciesSnapshot = await db.ref("/old/pharmacies").once('value');
                pharmaciesSnapshot.forEach(pharmacySnapshot => { pharmaciesList.push(pharmacySnapshot.key) });
                for (let i = 0; i < pharmaciesList.length; ++i) {
                    let oldPharmacyKey = pharmaciesList[i];
                    let medicinesSnapshot = await db.ref(`/old/pharmacies/${oldPharmacyKey}/medicines`).once('value');
                    medicinesSnapshot.forEach(medicineSnapshot => {
                        let oldMedicineKey = medicineSnapshot.key;
                        let oldMedicine = medicineSnapshot.val();
                        let oldEanIsNull = oldMedicine.ean === null && oldMedicine.ean === undefined;
                        let eanMapDoesntHaveEntry = !oldEanIsNull && (eanMap[oldMedicine.ean] === null || eanMap[oldMedicine.ean] === undefined);
                        if (oldEanIsNull || eanMapDoesntHaveEntry) {
                            let newMedicine = {
                                activePrinciple: protect(oldMedicine.activePrinciple),
                                AMU: protect(oldMedicine.applicationMeasurementUnit),
                                AMUPlural: protect(oldMedicine.aMUPlural),
                                PMU: protect(oldMedicine.presentationMeasurementUnit),
                                PMUPlural: protect(oldMedicine.pMUPlural),
                                cnpj: protect(oldMedicine.cnpj),
                                createdOn: new Date().toISOString(),
                                description: protect(oldMedicine.description),
                                description2: protect(oldMedicine.description2),
                                ean: protect(oldMedicine.ean),
                                laboratory: protect(oldMedicine.lab),
                                leafletURL: protect(oldMedicine.leafletURL),
                                name: protect(oldMedicine.productName),
                                photoURL: protect(oldMedicine.photoURL),
                                presentation: protect(oldMedicine.presentation),
                                stripe: protect(oldMedicine.stripe),
                                summary: protect(oldMedicine.summary),
                                therapeuticClass: protect(oldMedicine.therapeuticClass),
                                type: protect(oldMedicine.productType),
                            };
                            let newMedicineKey = db.ref("/new/medicines").push().key;
                            medicineMap[oldMedicineKey] = newMedicineKey;
                            if (eanMapDoesntHaveEntry)
                                eanMap[oldMedicine.ean] = newMedicineKey;
                            let promise = new Promise((resolve, reject) => {
                                console.log(`Refactoring medicine ${oldMedicineKey}`);
                                db.ref(`/new/medicines/${newMedicineKey}`).set(newMedicine).then(resolve());
                            });
                            promiseList.push(promise);
                        }
                    });
                }
                await Promise.all(promiseList);
            } catch (e) {
                console.error("[REFACTOR] Cloud function error: function refact 7");
                console.error(e);
                return;
            }
            // Refactor pharmacies x medicines relationship
            try {
                console.log("[REFACTOR] Refactoring supplies");
                let promiseList = [];
                let pharmaciesList = [];
                let pharmaciesSnapshot = await db.ref("/old/pharmacies").once('value');
                pharmaciesSnapshot.forEach(pharmacySnapshot => { pharmaciesList.push(pharmacySnapshot.key) });
                for (let i = 0; i < pharmaciesList.length; ++i) {
                    let oldPharmacyKey = pharmaciesList[i];
                    let medicinesSnapshot = await db.ref(`/old/pharmacies/${oldPharmacyKey}/medicines`).once('value');
                    medicinesSnapshot.forEach(medicineSnapshot => {
                        let oldMedicineKey = medicineSnapshot.key;
                        let newMedicineKey = medicineMap[oldMedicineKey];
                        let newPharmacyKey = pharmacyMap[oldPharmacyKey];
                        if (newMedicineKey === null || newMedicineKey === undefined) {
                            console.log(`[REFACTOR] Pharmacies x medicines: Medicine id='${oldMedicineKey}' was not found`);
                        } else if (newPharmacyKey === null || newPharmacyKey === undefined) {
                            console.log(`[REFACTOR] Pharmacies x medicines: Pharmacy id='${oldPharmacyKey}' was not found`);
                        } else {
                            let relKey = db.ref("/new/supplies").push().key;
                            let keysConcatenation = `${newMedicineKey}_x_${newPharmacyKey}`;
                            supplyMap[keysConcatenation] = relKey;
                            let prom = db.ref(`/new/supplies/${relKey}`).set({
                                createdOn: new Date().toISOString(),
                                pharmacyKey: newPharmacyKey,
                                medicineKey: newMedicineKey,
                                relationKey: keysConcatenation
                            });
                            promiseList.push(prom);
                        }
                    });
                }
                await Promise.all(promiseList);
            } catch (e) {
                console.error("[REFACTOR] Cloud function error: function refact 8");
                console.error(e);
                return;
            }
            // Refactor medicines x batches relationship
            try {
                console.log("[REFACTOR] Refactoring batches");
                let promiseList = [];
                let pharmaciesList = [];
                let medicinesList = [];
                let pharmaciesSnapshot = await db.ref("/old/pharmacies").once('value');
                pharmaciesSnapshot.forEach(pharmacySnapshot => { pharmaciesList.push(pharmacySnapshot.key) });
                for (let i = 0; i < pharmaciesList.length; ++i) {
                    let oldPharmacyKey = pharmaciesList[i];
                    let medicinesSnapshot = await db.ref(`/old/pharmacies/${oldPharmacyKey}/medicines`).once('value');
                    medicinesSnapshot.forEach(medicineSnapshot => { medicinesList.push({ pharmacyKey: oldPharmacyKey, medicineKey: medicineSnapshot.key }); });
                }
                for (let i = 0; i < medicinesSnapshot.length; ++i) {
                    let oldPharmacyKey = medicinesSnapshot[i].pharmacyKey;
                    let oldMedicineKey = medicinesSnapshot[i].medicineKey;
                    let batchesSnapshot = await db.ref(`/old/pharmacies/${oldPharmacyKey}/medicines/${oldMedicineKey}/batches`).once('value');
                    batchesSnapshot.forEach(batchSnapshot => {
                        let newMedicineKey = medicineMap[oldMedicineKey];
                        let newPharmacyKey = pharmacyMap[oldPharmacyKey];
                        if (newMedicineKey === null || newMedicineKey === undefined) {
                            console.log(`[REFACTOR] Batches: Medicine id='${oldMedicineKey}' was not found`);
                        } else if (newPharmacyKey === null || newPharmacyKey === undefined) {
                            console.log(`[REFACTOR] Batches: Pharmacy id='${oldPharmacyKey}' was not found`);
                        } else {
                            let oldBatchKey = batchSnapshot.key;
                            let newBatchKey = db.ref("/new/batches").push().key;
                            batchMap[oldBatchKey] = newBatchKey;
                            let supplyKey = supplyMap[`${newMedicineKey}_x_${newPharmacyKey}`];
                            let oldBatch = batchSnapshot.val();
                            let newValidity = convertDateToNewFormat(oldBatch.validity);
                            let newBatch = {
                                createdOn: new Date().toISOString(),
                                quantity: protect(oldBatch.currentQuantity),
                                validity: newValidity,
                                relationKey: `${supplyKey}_x_${newValidity}`,
                                supplyKey 
                            };
                            let prom = db.ref(`/new/batches/${newBatchKey}`).set(newBatch);
                            promiseList.push(prom);
                        }
                    });
                }
                await Promise.all(promiseList);
            } catch (e) {
                console.error("[REFACTOR] Cloud function error: function refact 8");
                console.error(e);
                return;
            }
            // Refactor pharmacies x user relationship
            try {
                console.log("[REFACTOR] Refactoring pharmacies x user");
                let promiseList = [];
                let pharmaciesList = [];
                let pharmaciesSnapshot = await db.ref("/old/pharmacies").once('value');
                pharmaciesSnapshot.forEach(pharmacySnapshot => { pharmaciesList.push(pharmacySnapshot.key) });
                for (let i = 0; i < pharmaciesList.length; ++i) {
                    let oldPharmacyKey = pharmaciesList[i];
                    let usersSnapshot = await db.ref(`/old/pharmacies/${oldPharmacyKey}/members`).once('value');
                    usersSnapshot.forEach(userSnapshot => {
                        let oldUserKey = userSnapshot.key;
                        let newUserKey = userMap[oldUserKey];
                        let newPharmacyKey = pharmacyMap[oldPharmacyKey];
                        let oldUser = userSnapshot.val();
                        if (newUserKey === null || newUserKey === undefined) {
                            console.log(`[REFACTOR] Pharmacies x user: User id='${oldUserKey}' was not found`);
                        } else if (newPharmacyKey === null || newPharmacyKey === undefined) {
                            console.log(`[REFACTOR] Pharmacies x user: Pharmacy id='${oldPharmacyKey}' was not found`);
                        } else {
                            let relKey = db.ref("/new/pharmacy_user").push().key;
                            let prom = db.ref(`/new/pharmacy_user/${relKey}}`).set({
                                currentUserKey: null,
                                permission: oldUser.owner ? "owner" : "manager",
                                pharmacyKey: newPharmacyKey,
                                relationKey: `${newUserKey}_x_${newPharmacyKey}`,
                                userKey: newUserKey
                            });
                            promiseList.push(prom);
                        }
                    });
                }
                await Promise.all(promiseList);
            } catch (e) {
                console.error("[REFACTOR] Cloud function error: function refact 9");
                console.error(e);
                return;
            }
            // Refactor currentPharmacyKey
            try {
                console.log("[REFACTOR] Refactoring currentPharmacyKey");
                let promiseList = [];
                let usersSnapshot = await db.ref("/old/users").once('value');
                usersSnapshot.forEach(userSnapshot => {
                    let oldUserKey = userSnapshot.key;
                    let oldUser = userSnapshot.val();
                    let oldPharmacyKey = oldUser.currentPharmacyKey;
                    if (!oldPharmacyKey) {
                        console.log(`[REFACTOR] User ${oldUserKey} does not have a pharmacy selected`);
                    } else if (!pharmacyMap[oldPharmacyKey]) {
                        console.log(`[REFACTOR] User ${oldUserKey} has a weird current pharmacy: ${oldPharmacyKey}`);
                    } else if (!userMap[oldUserKey]){
                        console.log(`[REFACTOR] User ${oldUserKey} does not have a correspondent`);
                    } else {
                        let newUserKey = userMap[oldUserKey];
                        let newPharmacyKey = pharmacyMap[oldPharmacyKey];
                        let keysConcatenation = `${newUserKey}_x_${newPharmacyKey}`;
                        let prom = db.ref(`/new/pharmacy_user`)
                                    .orderByChild('relationKey')
                                    .equalTo(keysConcatenation)
                                    .update({ currentUserKey: newUserKey });
                        promiseList.push(prom);
                    }
                });
            } catch (e) {
                console.error("[REFACTOR] Cloud function error: function refact 10");
                console.error(e);
                return;
            }
            // Clean database
            try {
                console.log("[REFACTOR] Cleaning database");
                let refactoredDatabaseSnapshot = await db.ref("/new").once('value');
                let refactoredDatabase = refactoredDatabaseSnapshot.val();
                await db.ref("/").set(refactoredDatabase);
            } catch (e) {
                console.error("[REFACTOR] Cloud function error: function refact 11");
                console.error(e);
                return;
            }
            console.log("Done!");
        }
        run();
        res.status(200).send("Ok");
    });
